package doomvr;

import java.awt.event.KeyEvent;
import java.util.List;

import javax.swing.JFrame;

import org.openimaj.image.ImageUtilities;
import org.openimaj.image.processing.face.detection.DetectedFace;
import org.openimaj.image.processing.face.detection.HaarCascadeDetector;

import com.github.sarxos.webcam.Webcam;
import com.github.sarxos.webcam.WebcamPanel;

import doomvr.model.Keyboard;
import doomvr.model.Ryjec;

public class Main {
	
	private static void terminate() {
		System.exit(0);
	}

	public static void main(String[] args) throws InterruptedException {
		final HaarCascadeDetector detector = new HaarCascadeDetector();
		List<DetectedFace> faces = null;

		Webcam webcam = Webcam.getDefault();
		webcam.setViewSize(Config.webcamDimension);

		WebcamPanel panel = new WebcamPanel(webcam);
		panel.setFPSDisplayed(true);
		panel.setDisplayDebugInfo(true);
		panel.setImageSizeDisplayed(true);
		panel.setMirrored(true);

		JFrame window = new JFrame("Test webcam panel");
		window.add(panel);
		window.setResizable(true);
		window.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		window.pack();
		window.setVisible(true);

		Thread.sleep(2000);
		Ryjec ryjec = new Ryjec();
		Keyboard keyboard = null;
		try {
			keyboard = new Keyboard();
		} catch (Exception e) {
			e.printStackTrace();
			terminate();
		}
		DetectedFace mainFace;
		while (webcam.isOpen()) {
			Thread.sleep(Config.DELAY);
			
			faces = detector.detectFaces(ImageUtilities.createFImage(panel.getImage()));
			
			try {
				try {
					mainFace = faces.get(0);
				} catch(NullPointerException ignore) {
					mainFace = null;
				}
				for(DetectedFace face : faces) {
					if(face.getBounds().getHeight() * face.getBounds().getWidth() > mainFace.getBounds().getHeight() * mainFace.getBounds().getWidth()) {
						mainFace = face;
					}
				}
				
				ryjec.setData(mainFace);
				
			} catch (IndexOutOfBoundsException ignore) {
				System.out.println("Ryjec wywalilo poza skale");
			} catch (NullPointerException e) {
				System.out.println("Bez ryjcow");
			} finally {
				//Debug
				System.out.print("Ratio="+ryjec.getRatio());
				System.out.print(" Size="+ryjec.getSize());
				
				//Move fw/bw
				System.out.print(" Move=");
				if(ryjec.getSize() >= Config.frontMovement) {
					keyboard.goForward();
					System.out.print("Przod");
				} else if (ryjec.getSize() <= Config.backMovement) {
					keyboard.goBackward();
					System.out.print("Tyl");
				} else {
					keyboard.releaseAllMovement();
					System.out.print("Stoi");
				}
				
				//Strafe
				System.out.print(" Strafe=");
				switch (ryjec.getPosition()) {
				case Ryjec.LEFTSIDE:
					keyboard.goLeft();
					System.out.println("Lewo");
					break;
				case Ryjec.RIGHTSIDE:
					keyboard.goRight();
					System.out.println("Prawo");
					break;
				case Ryjec.CENTER:
					keyboard.releaseAllStrafe();
					System.out.println("Srodek");
					break;
				}
			}
			if (faces == null) {
				System.out.println("No faces found in the captured image");
			}
		}
	}
}