package doomvr.model;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;

public class Keyboard extends Robot {

	public Keyboard() throws AWTException {
		super();
	}
	
	public void releaseAll() {
		this.keyRelease(KeyEvent.VK_COMMA);
		this.keyRelease(KeyEvent.VK_PERIOD);
		this.keyRelease(KeyEvent.VK_W);
		this.keyRelease(KeyEvent.VK_S);
	}
	
	public void releaseAllStrafe() {
		this.keyRelease(KeyEvent.VK_COMMA);
		this.keyRelease(KeyEvent.VK_PERIOD);
	}
	
	public void releaseAllMovement() {
		this.keyRelease(KeyEvent.VK_W);
		this.keyRelease(KeyEvent.VK_S);
	}
	
	public void goLeft() {
		this.releaseAll();
		this.keyPress(KeyEvent.VK_COMMA);
	}
	
	public void goRight() {
		this.releaseAll();
		this.keyPress(KeyEvent.VK_PERIOD);
	}
	
	public void goForward() {
		this.releaseAll();
		this.keyPress(KeyEvent.VK_W);
	}
	
	public void goBackward() {
		this.releaseAll();
		this.keyPress(KeyEvent.VK_S);
	}

}
