package doomvr.model;

import org.openimaj.image.processing.face.detection.DetectedFace;

import doomvr.Config;

public class Ryjec {
	private double x;
	private double y;
	private double ratio;
	private double size;
	public static final int LEFTSIDE = 0;
	public static final int CENTER = 1;
	public static final int RIGHTSIDE = 2;

	public int getPosition() {
		if (this.ratio < Config.rightSideRatio) {
			return Ryjec.RIGHTSIDE;
		} else if (this.ratio > Config.leftSideRatio) {
			return Ryjec.LEFTSIDE;
		} else {
			return Ryjec.CENTER;
		}
	}

	public double getX() {
		return x;
	}

	public void setData(DetectedFace mainFace) {
		this.x = mainFace.getBounds().getTopLeft().getX();
		this.setRatio((this.x / (Config.webcamDimension.getWidth())) + (Config.headRatio/2));
		this.setSize(mainFace.getBounds().getHeight() * mainFace.getBounds().getWidth());
	}

	public double getY() {
		return y;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getRatio() {
		return ratio;
	}

	public void setRatio(double ratio) {
		this.ratio = ratio;
	}
	
	public double getSize() {
		return this.size; 
	}
	
	public void setSize(double size) {
		this.size = size;
	}
}
