package doomvr;

import java.awt.Dimension;

public class Config {
	public static final int DELAY = 5;
	public static Dimension webcamDimension = new Dimension(176,144);

	public static double leftSideRatio = 0.55;
	public static double rightSideRatio = 0.45;
	public static double headRatio = 0.30;
	
	public static double backMovement = 3000.0;
	public static double frontMovement = 6000.0;
	
}
